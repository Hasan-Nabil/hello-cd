import { startServer } from './server';
import { printHelloWorld } from "./service";

async function startApp() {
    startServer();
}

startApp();