import * as express from "express";
import * as cors from "cors";
import { createServer } from "http";
import { Server } from "net";
import { printHelloWorld } from "./service";

const PORT = process.env["PORT"] || 80;
const app = express();
app.set("PORT", PORT);
app.use(cors());

app.get("/hello", printHelloWorld);

app.all("*", (req, res) => res.status(404).send());

const server = createServer(app);

export function startServer(): Server {
    return server.listen(PORT, () => {
        console.log("server listen on port ", PORT);
    });
}
