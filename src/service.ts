import { Request, Response } from "express";

export const printHelloWorld = async (req: Request, res: Response) => {
    res.write('Hello World');
    res.end();
}